import { join } from 'path'
import { DataSource } from 'typeorm'
import 'reflect-metadata'

export const AppDataSource = new DataSource({
  type: 'better-sqlite3',
  database: './cred_card.sqlite',
  synchronize: true,
  logging: true,
  entities: [join(__dirname, '../', '/**/*.entity{.ts,.js}')],
  migrations: [join(__dirname, '/migrations/**/*{.ts,.js}')],
  subscribers: [],
})

AppDataSource.initialize()
  .then(() => {
    console.log('Data Source has been initialized!')
  })
  .catch((err) => {
    console.error('Error during Data Source initialization', err)
  })
