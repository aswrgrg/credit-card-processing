import express from 'express'
import routes from './routes'
import config from './config'
export default class App {
  core: express.Application

  constructor() {
    this.core = express()
  }

  applyJsonMiddlewares() {
    this.core.set('trust proxy', true)
    this.core.use(express.json())
  }

  loadEnv(envFile?: string) {
    const dotenv = require('dotenv')

    if (!envFile) {
      envFile = `./${config.envFile}`
    }
    dotenv.config({
      path: envFile,
    })
  }

  healthchk() {
    this.core.get('/bank/v0/api/health_chk', (req, res) => {
      res
        .json({
          message: 'Health Check Ok',
        })
        .end()
    })
  }

  loadRoutes(path?: string) {
    this.core.use(path || '/bank/v0', routes)
  }
}
