import { RouteIface } from './config'
import * as creditCartProcessorController from '../controllers/creditCardProcessor.controller'

const creditCartProcessorApi: RouteIface[] = [
  {
    path: '',
    controller: creditCartProcessorController.listHandler,
    method: 'get',
  },
  {
    path: '',
    controller: creditCartProcessorController.createHandler,
    method: 'post',
  },
]
export default creditCartProcessorApi
