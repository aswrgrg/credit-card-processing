import routesConfig from './config'
import { Router, RequestHandler } from 'express'
import { ControllerActionIface } from '../controllers/base'
import JoiValidator from '../middlewares/joiValidator'

const composeRoute = (controller: ControllerActionIface) => {
  const middleWares: RequestHandler[] = []

  if (controller.bodySchema) {
    middleWares.push(JoiValidator(controller.bodySchema))
  }

  middleWares.push(controller.handler)

  return middleWares
}

const routes = Router()

Object.keys(routesConfig).forEach((k) => {
  const configs = routesConfig[k]
  configs.forEach((c) => {
    routes[c.method](`/${k}${c.path}`, composeRoute(c.controller))
  })
})

export default routes
