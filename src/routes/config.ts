import { ControllerActionIface } from '../controllers/base'
import creditCardProcessorApi from './creditCardProcessor.route'
export interface RouteIface {
  path: string
  method: 'get' | 'post' | 'put' | 'delete' | 'patch'
  controller: ControllerActionIface
}
interface RoutesConfigIface {
  [scope: string]: RouteIface[]
}

const routesConfig: RoutesConfigIface = {
  'api/credit_cards': creditCardProcessorApi,
}

export default routesConfig
