import { Handler } from 'express'
import joi from 'joi'
export interface ControllerActionIface {
  handler: Handler
  bodySchema?: joi.ObjectSchema<any>
}
