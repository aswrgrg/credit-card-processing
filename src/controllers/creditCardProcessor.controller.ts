import { ControllerActionIface } from './base'
import { Request, Response } from 'express'
import joi from 'joi'
import CreditCardProcessorService from '../services/creditCardProcessor.service'
import { AppDataSource } from '../db/data-source'
import { CreditCard } from '../entities/creditCard.entity'

export const listHandler: ControllerActionIface = {
  handler: async function (req: Request, res: Response) {
    const creditCards = await CreditCardProcessorService.list(req.query)
    res.status(200).send({ data: creditCards })
  },
}

export const createHandler: ControllerActionIface = {
  bodySchema: joi.object({
    name: joi.string().required(),
    card_number: joi.number().min(0).max(9999999999999999999).required(),
    limit: joi.number().required(),
    balance: joi.number().required(),
  }),
  handler: async function (req: Request, res: Response) {
    const { name, card_number, limit, balance } = req.body
    const creditCard = await AppDataSource.manager.findOneBy(CreditCard, { card_number })
    if (creditCard) {
      res.status(400).send({ error: 'Credit card already exist.' })
    } else {
      await CreditCardProcessorService.create({ name, card_number, limit, balance })
      res.status(200).send({ message: 'Credit card added successfully.' })
    }
  },
}
