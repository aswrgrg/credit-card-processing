import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class CreditCard {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Index({ unique: true })
  @Column()
  card_number: number

  @Column()
  limit: number

  @Column()
  balance: number
}
