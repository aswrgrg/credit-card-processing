import { AppDataSource } from '../db/data-source'
import { CreditCard } from '../entities/creditCard.entity'

type CreditCardInformation = {
  name: string
  card_number: number
  limit: number
  balance: number
}

const CreditCardProcessorService = {
  async create(postData: CreditCardInformation) {
    const { name, card_number, limit, balance } = postData
    const creditCard = new CreditCard()
    creditCard.name = name
    creditCard.card_number = card_number
    creditCard.limit = limit
    creditCard.balance = balance
    try {
      await AppDataSource.manager.save(creditCard)
    } catch (error) {
      if (error instanceof Error) {
        console.error(error.message)
      }
    }
  },

  async list(queryParams: { name?: string; card_number?: number }): Promise<CreditCardInformation[]> {
    const filters = {
      name: undefined,
      card_number: undefined,
    }
    if (queryParams.name) {
      filters.name = queryParams.name
    }
    if (queryParams.card_number) {
      filters.card_number = queryParams.card_number
    }
    return await AppDataSource.manager.find(CreditCard, { where: filters })
  },
}

export default CreditCardProcessorService
