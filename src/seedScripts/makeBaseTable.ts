import db from '../utils/database'
db.run(
  'CREATE TABLE credit_card( \
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
            name NVARCHAR(20)  NOT NULL,\
            card_number INTEGER  NOT NULL,\
            limit FLOAT,\
        )',
  (err) => {
    if (err) {
      console.log('Table already exists.')
    }
  }
)
