import { NextFunction, Response } from 'express'
import { listHandler, createHandler } from '../../../src/controllers/creditCardProcessor.controller'
import CreditCardProcessorService from '../../../src/services/creditCardProcessor.service'
import { AppDataSource } from '../../../src/db/data-source'

jest.mock('../../../src/services/creditCardProcessor.service')
jest.mock('../../../src/db/data-source')

beforeEach(async () => {
  jest.resetAllMocks()
})

afterEach(() => {
  jest.resetAllMocks()
})

describe('CreditCard Processor Controller', () => {
  describe('GET: List CreditCards Handler', () => {
    it('should at least call CreditCardProcessorService once', async () => {
      const req: any = {}
      const res = {} as unknown as Response
      res.send = jest.fn()
      res.status = jest.fn(() => res)
      const next = jest.fn()

      CreditCardProcessorService.list = jest.fn().mockImplementation(() => [
        { id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 },
        { id: 1, name: 'Sujan', card_number: 123, limit: 100, balance: 100 },
      ])

      listHandler.handler(req, res as Response, next as NextFunction)
      expect(CreditCardProcessorService.list).toBeCalledTimes(1)
    })

    it('should return list credit cards', async () => {
      const req: any = {}
      const res = {} as unknown as Response
      res.send = jest.fn()
      res.status = jest.fn(() => res)
      const next = jest.fn()

      CreditCardProcessorService.list = jest.fn().mockImplementation(() => [
        { id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 },
        { id: 1, name: 'Sujan', card_number: 123, limit: 100, balance: 100 },
      ])

      const expectedReturn = [
        { id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 },
        { id: 1, name: 'Sujan', card_number: 123, limit: 100, balance: 100 },
      ]
      listHandler.handler(req, res as Response, next as NextFunction)
      const list = await CreditCardProcessorService.list({})
      expect(list).toEqual(expectedReturn)
    })

    it('should return filter credit card by name', async () => {
      const req: any = { query: { name: 'Amar' } }
      const res = {} as unknown as Response
      res.send = jest.fn()
      res.status = jest.fn(() => res)
      const next = jest.fn()

      CreditCardProcessorService.list = jest
        .fn()
        .mockImplementation(() => [{ id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 }])
      const expectedReturn = [{ id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 }]

      listHandler.handler(req, res as Response, next as NextFunction)
      const list = await CreditCardProcessorService.list(req.query)
      expect(list).toEqual(expectedReturn)
    })
  })
  describe('POST: Insert Credit Card handler', () => {
    it('should not insert credit card if already exist', async () => {
      const req: any = { body: {} }
      const res = {} as unknown as Response
      res.send = jest.fn()
      res.status = jest.fn(() => res)
      const next = jest.fn()

      CreditCardProcessorService.create = jest.fn()

      AppDataSource.manager.findOneBy = jest
        .fn()
        .mockImplementation(() => [{ id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 }])
      createHandler.handler(req, res as Response, next as NextFunction)
      expect(CreditCardProcessorService.create).toBeCalledTimes(0)
    })

    it('should insert new credit card', async () => {
      const req: any = { body: { name: 'Amar', card_number: 123, limit: 100, balance: 100 } }
      const res = {} as unknown as Response
      res.send = jest.fn()
      res.status = jest.fn(() => res)
      const next = jest.fn()

      createHandler.handler(req, res as Response, next as NextFunction)
      CreditCardProcessorService.create = jest.fn()
      AppDataSource.manager.findOneBy = jest.fn().mockImplementation()
      expect(await CreditCardProcessorService.create).toBeCalledTimes(1)
      expect(await CreditCardProcessorService.create).toBeCalledWith(req.body)
    })
  })
})
