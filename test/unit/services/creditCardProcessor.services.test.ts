import CreditCardProcessorService from '../../../src/services/creditCardProcessor.service'

import { AppDataSource } from '../../../src/db/data-source'
import { CreditCard } from '../../../src/entities/creditCard.entity'

jest.mock('../../../src/db/data-source')

describe('Credit Card Processor Service', () => {
  describe('Create/Insert new credit card', () => {
    it('should call AppDataSource.manager.save', async () => {
      const postData = {
        name: 'Amar',
        card_number: 123,
        limit: 100,
        balance: 100,
      }
      AppDataSource.manager.save = jest.fn().mockImplementation()
      CreditCardProcessorService.create(postData)
      expect(AppDataSource.manager.save).toBeCalledTimes(1)

      expect(AppDataSource.manager.save).toBeCalledWith(postData)
    })
  })
  describe('List all credit cards', () => {
    it('should call AppDataSource.manager.find', async () => {
      const queryParams = {
        where: {
          name: 'Amar',
          card_number: 123,
        },
      }
      AppDataSource.manager.find = jest.fn().mockImplementation()
      CreditCardProcessorService.list({ name: 'Amar', card_number: 123 })
      expect(AppDataSource.manager.find).toBeCalledTimes(1)
      expect(AppDataSource.manager.find).toBeCalledWith(CreditCard, queryParams)
    })
    it('should return credit card lists', async () => {
      const queryParams = {
        where: {
          name: 'Amar',
          card_number: 123,
        },
      }
      AppDataSource.manager.find = jest
        .fn()
        .mockImplementation(() => [{ id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 }])
      CreditCardProcessorService.list({ name: 'Amar' })
      const creditCardList = await AppDataSource.manager.find(CreditCard, queryParams)
      expect(creditCardList).toEqual([{ id: 1, name: 'Amar', card_number: 123, limit: 100, balance: 100 }])
    })
  })
})
