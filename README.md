# Credit Card Processor

A demo credit card processing application to store credit card details.
Postman collection Credit Card.postman_collection.json file is included in the root of the project that contains the endpoints for the application which can be imported in the postman.

## Technology Used

    $ Nodejs
    $ TypeScript
    $ Express.js
    $ Sqlite
    $ TypeOrm

The following node version was used to build the application

    $ node --version
    v18.9.0

## Install

    $ git clone git@gitlab.com:aswrgrg/credit-card-processing.git
    $ cd PROJECT_TITLE
    $ npm install

## Running the project

    $ npm run start:dev

## Unit testing the project

     $ npm run test
